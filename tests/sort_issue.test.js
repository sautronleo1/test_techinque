const sort_issue = require('../src/sort_issue.js');

var json_invalid_state = `[
    {
        "id": 2,
        "state": "opened",
        "updated_at": "2023-06-21T09:29:40.085Z"
    },
    {
        "id": 1,
        "state": "closed",
        "updated_at": "2023-06-21T09:28:00.038Z"
    },
    {
        "id": 3,
        "state": "null",
        "updated_at": "2023-06-21T09:31:20.492Z"
    }
]`;

var json_no_updated_at_opened = `[
    {
        "id": 2,
        "state": "opened",
        "updated_at": "2023-06-21T09:29:40.085Z"
    },
    {
        "id": 1,
        "state": "closed",
        "updated_at": "2023-06-21T09:28:00.038Z"
    },
    {
        "id": 3,
        "state": "opened"
    }
]`;

var json_no_updated_at_closed = `[
    {
        "id": 2,
        "state": "opened",
        "updated_at": "2023-06-21T09:29:40.085Z"
    },
    {
        "id": 1,
        "state": "closed"
    },
    {
        "id": 3,
        "state": "opened",
        "updated_at": "2023-06-21T09:31:20.492Z"
    }
]`;

test('sort_issues', () => {
    var result = sort_issue.sort_issues(JSON.parse(sort_issue.json));
    expect(result).toEqual({
        "opened": [
            {
                "id": 3,
                "state": "opened",
                "updated_at": "2023-06-21T09:31:20.492Z"
            },
            {
                "id": 2,
                "state": "opened",
                "updated_at": "2023-06-21T09:29:40.085Z"
            }
        ],
        "closed": [
            {
                "id": 1,
                "state": "closed",
                "updated_at": "2023-06-21T09:28:00.038Z"
            },
            {
                "id": 4,
                "state": "closed",
                "updated_at": "2023-06-21T09:27:10.763Z"
            }
        ]
    });
});

test('sort_issues_invalid_state', () => {
    var result = sort_issue.sort_issues(JSON.parse(json_invalid_state));
    expect(result).toEqual(null);
});

test('sort_issues_no_updated_at', () => {
    var result = sort_issue.sort_issues(JSON.parse(json_no_updated_at_opened));
    expect(result).toEqual(null);
});

test('sort_issues_no_updated_at', () => {
    var result = sort_issue.sort_issues(JSON.parse(json_no_updated_at_closed));
    expect(result).toEqual(null);
});
