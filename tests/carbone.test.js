const carbone = require('../src/Carbone_Basics.js');

test('Test carbone api set template', () => {
    carbone.api.setTemplate("./assets/template.docx");
    expect(carbone.api.template).toBe("./assets/template.docx");
});

test('Test carbone api set output', () => {
    carbone.api.setOutput("./output.pdf");
    expect(carbone.api.output).toBe("./output.pdf");
});

test('Test carbone api set data', () => {
    carbone.api.setData({"test": "test"});
    expect(carbone.api.data).toStrictEqual({"test": "test"});
});
