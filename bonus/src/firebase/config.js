// Import the functions you need from the SDKs you need
import { initializeApp, getApps } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDW4q2pU8bRd7ABwXEefBc6Y49dIlpQZ1g",
  authDomain: "bonus-test-technique.firebaseapp.com",
  projectId: "bonus-test-technique",
  storageBucket: "bonus-test-technique.appspot.com",
  messagingSenderId: "310960974761",
  appId: "1:310960974761:web:011ad8d955a592340072b8",
  measurementId: "G-6YTRZ3VLN2"
};

// Initialize Firebase
let firebase_app = getApps().length === 0 ? initializeApp(firebaseConfig) : getApps()[0];

export default firebase_app;
