const fetch = require('node-fetch');
const carbone = require('carbone');
const fs = require('fs');

class task1 {
  constructor() {
    this.template = "";
  }
  setTemplate(template) {
    this.template = template;
  }
  async generateReport(req, res) {
    if (this.template == "") {
      return res.status(500).send("Template not found");
    }
    const resp = await fetch(`https://render.carbone.io/render/${this.template}`, {
      method: "POST",
      headers: {
        'Authorization': process.env.CARBONE_API_KEY,
        'Content-Type': 'application/json',
        'Carbone-Version': '4'
      },
      body: JSON.stringify({
        convertTo: "pdf",
        data: req
      })
    }).then((resp) => resp.json());
    if (resp && resp.success == true && resp.data && resp.data.renderId) {
      res.send(`https://render.carbone.io/render/${resp.data.renderId}`).status(200);
      return (`https://render.carbone.io/render/${resp.data.renderId}`);
    } else if (resp && resp.error) {
      console.log(resp.error);
      res.send(resp.error).status(400);
      return (resp.error);
    }
  }
}

class task4 {
  constructor() {
    this.template = "";
    this.output = "";
    this.data = undefined;
  }
  setTemplate(template) {
    this.template = template;
  }
  setOutput(output) {
    this.output = output;
  }
  setData(data) {
    this.data = data;
  }
  generateReport() {
    if (this.template == "" || this.output == "" || this.data == undefined) {
      return null;
    }
      var options = {
        convertTo: 'pdf'
    };
    carbone.render(this.template, this.data, options, (err, result) => {
      if (err) {
        console.log(err);
        return null;
      }
      fs.writeFileSync(this.output, result);
    }
    );
  }
}
var task = new task1();
var api = new task4();

module.exports = { task, api }
