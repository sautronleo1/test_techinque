const express = require('express');
var app = express();
require('dotenv').config()
const port = process.env.PORT;
const api = require("./api/api.js");
const carbone = require('./api/carbone.js');
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/api/v1", api.router)
app.use("/api/v2", carbone.router);

app.listen( port, () => {
    console.log("listening on port 8080")
});

