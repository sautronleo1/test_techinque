const carbone_api = require('../Carbone_Basics.js');
const gitlab_api = require('./gitlab_api.js');
const express = require('express');
const router = express.Router();

const generate = (req, res) => {
    if (req.body == undefined) {
        return res.status(400).send("Bad Request");
    }
    carbone_api.api.setTemplate("./assets/template.docx");
    carbone_api.api.setOutput("./output.pdf");
    carbone_api.api.setData(req.body);
    try {
        carbone_api.api.generateReport();
    }
    catch (err) {
        console.log(err);
        return res.status(500).send("Internal Server Error");
    }
    res.send("Okay").status(200);
}

const generateIssue = (req, res) => {
    if (req.body == undefined) {
        return res.status(400).send("Bad Request");
    }
    gitlab_api.api.generateIssueTask4(res);
}

router.get("/pdf", generate)
router.get("/pdf/gitlab", generateIssue)

module.exports = { router }
