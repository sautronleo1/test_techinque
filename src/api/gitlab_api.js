const fetch = require('node-fetch');
const carbone = require('../Carbone_Basics.js');


class gitlab_api {
    constructor() {
    }
    async generateIssue(res) {
        try {
            const issuesResponse = await fetch(
                `${process.env.GITLAB_API_BASE_URL}/projects/${encodeURIComponent(process.env.GITLAB_PROJECT_PATH)}/issues`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${process.env.GITLAB_API_KEY}`,
                    },
                }
            );
            if (issuesResponse.ok) {
                carbone.task.setTemplate("2d4818052f1a31a5594f16370d8266bf49c1f9dfbf8f2940d7c9049bc75d1722");
                const issuesData = await issuesResponse.json();
                carbone.task.generateReport(issuesData, res);
            } else {
                console.error(
                    `Failed to fetch issues from GitLab: ${issuesResponse.status} ${issuesResponse.statusText}`
                );
            }
        } catch (error) {
            console.error('An error occurred:', error.message);
        }
    }
    async generateIssueTask4(res) {
        try {
            const issuesResponse = await fetch(
                `${process.env.GITLAB_API_BASE_URL}/projects/${encodeURIComponent(process.env.GITLAB_PROJECT_PATH)}/issues`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${process.env.GITLAB_API_KEY}`,
                    },
                }
            );
            if (issuesResponse.ok) {
                carbone.api.setTemplate("./assets/Document2.docx");
                const issuesData = await issuesResponse.json();
                carbone.api.setData(issuesData);
                carbone.api.setOutput("result.pdf");
                carbone.api.generateReport();
            } else {
                console.error(
                    `Failed to fetch issues from GitLab: ${issuesResponse.status} ${issuesResponse.statusText}`
                );
            }
        } catch (error) {
            console.error('An error occurred:', error.message);
        }
    }
}

api = new gitlab_api();

module.exports = { api }
