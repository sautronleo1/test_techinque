const carbone = require('../Carbone_Basics.js');
const express = require('express');
const router = express.Router();
const gitlab = require('./gitlab_api.js');

const generate = (req, res) => {
    if (req.body == undefined) {
        return res.status(400).send("Bad Request");
    }
    carbone.task.setTemplate("9083d050ce7d24c91dc17922685a6c346c1fbf6739fe4a8c9ae8b324283b6045");
    carbone.task.generateReport(req.body, res);
}

const generateIssue = (req, res) => {
    if (req.body == undefined) {
        return res.status(400).send("Bad Request");
    }
    try {
        gitlab.api.generateIssue();
        res.send("Okay").status(200);
    }
    catch (err) {
        console.log(err);
        return res.status(500).send("Internal Server Error");
    }
}

router.get("/pdf", generate)
router.get("/pdf/gitlab", generateIssue)

module.exports = { router }

