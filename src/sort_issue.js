const json = `[
    {
      "id": 2,
      "state": "opened",
      "updated_at": "2023-06-21T09:29:40.085Z"
    },
    {
      "id": 1,
      "state": "closed",
      "updated_at": "2023-06-21T09:28:00.038Z"
    },
    {
      "id": 3,
      "state": "opened",
      "updated_at": "2023-06-21T09:31:20.492Z"
    },
    {
      "id": 4,
      "state": "closed",
      "updated_at": "2023-06-21T09:27:10.763Z"
    }
  ]`;


function sort_json_by_updated_at(json) {
  if (json.length == 1) {
    return json.updated_at == undefined ? null : json;
  }
  try {
    json.sort((a, b) => {
      if (a.updated_at == undefined || b.updated_at == undefined) {
        throw "Error: invalid json";
      }
      const dateA = new Date(a.updated_at);
      const dateB = new Date(b.updated_at);
      return dateB - dateA;
    });
    return json
  }
  catch (err) {
    console.log(err);
    return null;
  }
}

const sort_issues = (issues) => {
  var issues_open = [];
  var issues_close = [];

  for (var i = 0; i < issues.length; i++) {
    if (issues[i].state == "opened") {
      issues_open.push(issues[i]);
    } else if (issues[i].state == "closed") {
      issues_close.push(issues[i]);
    } else {
      console.log("Error: invalid state");
      return null;
    }
  }
  issues_open = sort_json_by_updated_at(issues_open);
  if (issues_open == null) {
    return null;
  }
  issues_close = sort_json_by_updated_at(issues_close);
  if (issues_close == null) {
    return null;
  }
  issues = { "opened": issues_open, "closed": issues_close }
  return issues;
}

sort_issues(JSON.parse(json));

module.exports = { sort_issues, json };
